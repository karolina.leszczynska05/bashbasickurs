#!/bin/bash

clear

trap 'echo " - Prosze nacisnij Q by wyjsc.."' SIGINT SIGTERM SIGTSTP

while [ "$WYB" != "Q" ]; do
    echo "Main Menu"
    echo "========="
    echo "1) Format"
    echo "2) Ustawinia"
    echo "3) Dane"
    echo "Q) Wyjscie"
    echo ""

    read -r WYB
    clear
done