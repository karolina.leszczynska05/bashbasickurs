#!/bin/bash

FILE="file.txt"

read -p "Podaj rozdzielacz: (Delimiter)" -n1 DELIM
echo
IFS="$DELIM"

while read -r CPU MEMORY DISK; do
    echo "CPU $CPU "
    echo "Memory: $MEMORY"
    echo "disk: $DISK"
done <"$FILE"