#!/bin/bash

#Bash Expansion Brace
#echo a{d,c,b}e

var=01234567890abcdefgh

echo ${var:7:2}

set -- 1 2 3 4 5 6 7 8 9 0 a b c d e f g h

echo ${@: -7:2}

var2=$(ls -la ./)

echo $var2