#!/bin/bash

func () {
   local var="example"
   global_var="1"
   var1="2"

   echo "Hej - jestem $var"
}

func

echo "$var"
echo "$global_var"
echo "$var1"