#!/bin/bash

wc -l -x plik.txt 2>/dev/null

if [[ "$?" -eq 0 ]]
then 
    echo "Polecenie wykonalo sie prawidlowo, oto zawartosc pliku"
    cat plik.txt
else
    echo "Nie udalo sie wykonac prawidlowo polecenia"
    exit 11
fi