#!/bin/bash

#declare -A MYARRAY
#MYARRAY[imie]="piotr"
#MYARRAY[nazwisko]="koska"
#MYARRAY[tel]="+48123456789"

#echo "${MYARRAY[@]}" # Poprawna deklaracja
#echo "Zła deklaracja" MYARRAY[key]
#echo "Zła deklaracja" $MYARRAY[key]

#declare -A MYMAP
#MYMAP=( [imie]="Monika" [nazwisko]="Koska" [tel]="+48987654321" )
#echo "Moja MYMAP: ${MYMAP[@]}"
#MYMAP[drugie_imie]="Katarzyna"
#echo "Moja MYMAP: ${MYMAP[@]}"

# K=baz
# declare -A MYARRAY_WITH_VAR[$K]=quux
# echo ${MYARRAY_WITH_VAR[$K]}
# echo ${MYARRAY_WITH_VAR[baz]}

# declare -A MYARR
# MYARR[foo A]="bar B"
# MYARR["config X"]='config Y'
# echo ${MYARR[@]}
# echo ${MYARR[foo A]}
# MYARR['foo B']="bar B"
# MYARR['config Y']='config X'
# echo ${MYARR[@]}
# echo ${MYARR[foo B]}

# declare -A ARRMISS
# ARRMISS[foo]=bar

# echo ${ARRMISS[missing]}

# if [ ${ARRMISS[foo]+_} ]; then echo "Istnieje"; else echo "Nie istnieje"; fi
# if [ ${ARRMISS[missing]+_} ]; then echo "Istnieje"; else echo "Nie istnieje"; fi

declare -A MYARR_LOOP=( [foo a]="one" [foo b]="two" [foo c]="three" [foo]="four")
echo "${MYARR_LOOP[@]}"

for K in "${MYARR_LOOP[@]}"; do echo "example 1" $K; done # wartosc
echo
for K in "${!MYARR_LOOP[@]}"; do echo "example 2" $K; done # klucz
echo
for K in ${!MYARR_LOOP[@]}; do echo "example 3" $K; done # klucz wartosc niepoprawnie
echo
echo

for K in "${!MYARR_LOOP[@]}"; do echo $K --- ${MYARR_LOOP[$K]}; done # klucz wartosc prowanie

unset MYARR_LOOP['foo']

echo "${MYARR_LOOP[@]}"

echo rozmiar ${#MYARR_LOOP[@]}

KEYS=("${!MYARR_LOOP[@]}")
echo "index: " ${KEYS[0]}

echo "wartosc indexu:" ${MYARR_LOOP[${KEYS[0]}]}

for (( I=0; $I < ${#MYARR_LOOP[@]}; I+=1 )); do KEY=${KEYS[$I]};  echo $KEY --- ${MYARR_LOOP[$KEY]}; done