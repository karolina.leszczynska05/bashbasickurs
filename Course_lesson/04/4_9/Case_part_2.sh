#!/bin/bash

while :
do

    echo "1) Stowrzenie folderu o nazwie Kurs"
    echo "2) Swtorzenie pliku txt w folderze Kurs"
    echo "3) Dopisanie zawartosci do pliku "
    echo "4) Wyswietlenie zawartosci pliku"
    echo "5) Zakoczenie skryptu"

    echo

    echo "Wybierz opcje z listy"
    read zmienna
    case "$zmienna" in
    "1") mkdir Kurs; echo "Stworzono folder Kurs" ;;
    "2") if [ -d Kurs ]; then touch ./Kurs/mojplik.txt; else echo "Nie ma folderu Kurs , wybierz opcje 1"; fi ;;
    "3") echo "Co checesz wpisac do pliku" ; read napis ; echo $napis > ./Kurs/mojplik.txt ;;
    "4") echo "Oto zawartosc pliku "; cat ./Kurs/mojplik.txt ;;
    "5") echo "Skrypt zakonczyl prace"; break
    esac
done
