#!/bin/bash

# Tablica obrazów systemu linux w roznych wersjach.
declare -A LINUX_DISTRO_ARRAY=( ["Ubuntu 16.04"]="https://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img" ["Ubuntu 18.04"]="https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img" ["Ubuntu 20.04"]="https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img" ["Quit"]="Quit")

# Pobieranie obrazu pliku systemu
while true
do
    select distro in "${!LINUX_DISTRO_ARRAY[@]}"
    do 
        case $distro in
            "Ubuntu 16.04") 
              echo ${distro};
              filename=${distro// /}
              wget ${LINUX_DISTRO_ARRAY["${distro}"]} -q --show-progress -O ./${filename,,}.img 
              echo; echo;
              read -p "Pobranie zakonczone! Nacisnij Enter"
              break ;;
            "Ubuntu 18.04") 
              echo ${distro};
              filename=${distro// /}
              wget ${LINUX_DISTRO_ARRAY["${distro}"]} -q --show-progress -O ./${filename,,}.img
              echo; echo;
              read -p "Pobranie zakonczone! Nacisnij Enter"
              break ;;
            "Ubuntu 20.04") 
              echo ${distro}; 
              filename=${distro// /}
              wget ${LINUX_DISTRO_ARRAY["${distro}"]} -q --show-progress -O ./${filename,,}.img
              echo; echo;
              read -p "Pobranie zakonczone! Nacisnij Enter"
              break ;;
            "Quit") exit 0 ;;
            *) echo "Nie rozpoznawalna wersja distro."; break ;;
        esac
    done
clear
done