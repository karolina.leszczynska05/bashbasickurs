#!/bin/bash


if [ -d TestIf ]
then
    if [ -e ./TestIf/plik1.txt ]
    then
        echo "Plik1.txt istnieje dopisuje ciag znakow do pliku"
        echo "Hello Piotr w pliku 1" > ./TestIf/plik1.txt
    elif [ -x ./TestIf/plik2.txt ]
    then
        echo "Plik2.txt jest plikiem wykonywalnym"
    else
        echo "Nie udalo sie wykonac operacji na plikach"
    fi
else
    echo "Nie ma folderu TestIf i jego plikow"
fi
