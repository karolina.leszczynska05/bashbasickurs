#!/bin/bash

#1. Wykorzystanie case
#2. For
#3. While
#4. Until

#------ skrypt -------

PS3='Wybierz opcje: '
option=("1" "2" "3" "Q")
select opt in "${option[@]}"
do
    case $opt in
        "1")
          echo "Until"
          until git pull &> /dev/null
          do
            echo "Czekam na synchronizacje repo"
            sleep 0.5
          done
          echo -e "\n Repozytorium synchronizowane."
          ;;
        "2")
          echo "For Loop"
          sudo apt-get update
          if [ ! -d "./log" ] 
          then
            mkdir ./log
          fi
          for software_var in "tmux" "htop" "curl"
          do
            sudo apt-get install $software_var | tee -a ./log/log-${software_var}.log
          done
          ;;
        "3")
          echo "While"
          file=/etc/resolv.conf
          while IFS=' ' read -r f1 f2
          do
            echo "Pole1: $f1 ==> Pole2: $f2"
          done < "$file"
          ;;
        [Qq])
          echo "Wyjscie"
          break
          ;;
        *)
          echo "Zła opcja"
          ;;
    esac
done