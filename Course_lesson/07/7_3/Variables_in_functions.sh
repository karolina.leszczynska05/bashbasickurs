#!/bin/bash


ZMIENNA_GLOBALNA="987654321"

funkcja1(){

    echo "Wartosc zmiennej globalnej to: $ZMIENNA_GLOBALNA"
}

funkcja2(){

    local ZMIENNA_LOKALNA="123456789"
    echo "Wartosc zmiennej lokalnej funkcji to : $ZMIENNA_LOKALNA"

}



echo "Przed wywolaniem"
echo "Zmienna globalna ma wartosc : $ZMIENNA_GLOBALNA"
echo "Zmienna lokalna w funkcji 2 ma wartosc: $ZMIENNA_LOKALNA"
echo
funkcja1
funkcja2
echo
echo "Wartosc zmiennnych po wywolaniu"
echo "Zmienna globalna ma wartosc : $ZMIENNA_GLOBALNA"
echo "Zmienna lokalna w funkcji 2 ma wartosc: $ZMIENNA_LOKALNA"
